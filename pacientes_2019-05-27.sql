# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.25)
# Base de Dados: pacientes
# Tempo de Geração: 2019-05-27 19:29:50 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela endereco
# ------------------------------------------------------------

DROP TABLE IF EXISTS `endereco`;

CREATE TABLE `endereco` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `endereco_tipo` char(3) NOT NULL DEFAULT '',
  `endereco_CEP` varchar(10) NOT NULL DEFAULT '',
  `endereco_UF` char(2) NOT NULL DEFAULT '',
  `endereco_cidade` varchar(60) NOT NULL DEFAULT '',
  `endereco_bairro` varchar(60) NOT NULL DEFAULT '',
  `endereco_rua` varchar(60) NOT NULL DEFAULT '',
  `endereco_numero` int(11) NOT NULL,
  `endereco_complemento` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `endereco` WRITE;
/*!40000 ALTER TABLE `endereco` DISABLE KEYS */;

INSERT INTO `endereco` (`id`, `endereco_tipo`, `endereco_CEP`, `endereco_UF`, `endereco_cidade`, `endereco_bairro`, `endereco_rua`, `endereco_numero`, `endereco_complemento`)
VALUES
	(8,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(10,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(11,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(12,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(13,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(14,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(15,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(34,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(35,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(36,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(37,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(38,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B'),
	(39,'RES','84010-280','PR','Ponta Grossa','Centro','Cel Dulcidio',253,'51B');

/*!40000 ALTER TABLE `endereco` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela paciente
# ------------------------------------------------------------

DROP TABLE IF EXISTS `paciente`;

CREATE TABLE `paciente` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `paciente_nome_completo` varchar(100) DEFAULT NULL,
  `paciente_nome_social` varchar(100) DEFAULT NULL,
  `paciente_nome_mae` varchar(100) DEFAULT NULL,
  `paciente_nome_pai` varchar(100) DEFAULT NULL,
  `paciente_data_nascimento` date DEFAULT NULL,
  `paciente_sexo` text,
  `paciente_rg` text,
  `paciente_cpf` text,
  `paciente_rg_uf` text,
  `paciente_email` varchar(50) DEFAULT NULL,
  `insert_time` timestamp NULL DEFAULT NULL,
  `alter_time` timestamp NULL DEFAULT NULL,
  `endereco_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_endereco` (`endereco_id`),
  CONSTRAINT `fk_endereco` FOREIGN KEY (`endereco_id`) REFERENCES `endereco` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `paciente` WRITE;
/*!40000 ALTER TABLE `paciente` DISABLE KEYS */;

INSERT INTO `paciente` (`id`, `paciente_nome_completo`, `paciente_nome_social`, `paciente_nome_mae`, `paciente_nome_pai`, `paciente_data_nascimento`, `paciente_sexo`, `paciente_rg`, `paciente_cpf`, `paciente_rg_uf`, `paciente_email`, `insert_time`, `alter_time`, `endereco_id`)
VALUES
	(12,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-15 02:07:17','2019-05-15 02:07:17',12),
	(16,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-15 02:07:17','2019-05-15 02:07:17',12),
	(17,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-15 02:07:17','2019-05-15 02:07:17',34),
	(18,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-15 02:07:17','2019-05-15 02:07:17',35),
	(19,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-15 02:07:17','2019-05-15 02:07:17',36),
	(20,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-22 12:54:28','2019-05-22 12:54:28',37),
	(21,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-21 09:55:34','2019-05-21 09:55:34',38),
	(22,'Filipe Franchini Hruba','Filipe Franchini','Anileda Franchini','Josemar Hruba','1995-10-06','M','973274274','05178571975','PR','filipe@ade.ag','2019-05-21 09:56:16','2019-05-21 09:56:16',39);

/*!40000 ALTER TABLE `paciente` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela telefone
# ------------------------------------------------------------

DROP TABLE IF EXISTS `telefone`;

CREATE TABLE `telefone` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `numero` int(11) DEFAULT NULL,
  `tipo` varchar(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;




/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
