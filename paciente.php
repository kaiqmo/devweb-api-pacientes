<?php

require_once("conn.php");
date_default_timezone_set('America/Sao_Paulo');

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    try {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->beginTransaction();
        $json_str = file_get_contents('php://input');

        if (!empty($json_str)) {
            $json_obj = json_decode($json_str);

            $json_paciente = $json_obj->paciente;
            $json_enderecos = $json_obj->enderecos;
            $json_telefones = $json_obj->telefones;

            $queryPaciente = "INSERT into paciente (";

            foreach ($json_paciente as $key => $value) {
                $queryPaciente = $queryPaciente . "$key, ";
            }
            $queryPaciente = rtrim($queryPaciente, ', ');
            $queryPaciente = $queryPaciente . ", insert_time, alter_time) VALUES (";

            foreach ($json_paciente as $key => $value) {
                $queryPaciente = $queryPaciente . ":$key, ";
            }

            $queryPaciente = rtrim($queryPaciente, ', ');
            $queryPaciente = $queryPaciente . ", :insert_time, :alter_time);";

            $stmtPaciente = $pdo->prepare($queryPaciente);

            foreach ($json_paciente as $key => $val) {
                $stmtPaciente->bindValue(":$key", $val);
                // echo "$key $val<br>";
            }

            $time = date("Y-m-d h:i:s");
            $stmtPaciente->bindValue(":insert_time", $time);
            $stmtPaciente->bindValue(":alter_time", $time);

            if ($stmtPaciente->execute()) {
                $paciente_id = $pdo->lastInsertId();
                echo "<br>Paciente inserido... id $paciente_id<br>";

                //INESERINDO ENDEREÇOS
                $params = [];
                foreach ($json_enderecos[0] as $key => $value) {
                    array_push($params, $key);
                }

                $queryEndereco = "INSERT INTO endereco (";
                foreach ($params as $param) {
                    $queryEndereco = $queryEndereco . "$param, ";
                }

                $queryEndereco = $queryEndereco . "endereco_paciente ) VALUES (";

                foreach ($params as $param) {
                    $queryEndereco = $queryEndereco . ":$param, ";
                }
                $queryEndereco = $queryEndereco . ":endereco_paciente );";


                $stmtEndereco = $pdo->prepare($queryEndereco);

                foreach ($json_enderecos as $endereco) {

                    foreach ($endereco as $key => $value) {
                        $stmtEndereco->bindValue(":$key", $value);
                    }
                    $stmtEndereco->bindValue(":endereco_paciente", $paciente_id);
                    if ($stmtEndereco->execute()) {
                        $lastID = $pdo->lastInsertID();
                        echo "Endereço cadastrado. ID: $lastID <br>";
                    } else {
                        echo "Erro na inserção de endereço<br>";
                        throw new Exception("Erro na inserção de endereço<br>");
                    }
                }

                //INSERINDO TELEFONES

                $params = [];
                foreach ($json_telefones[0] as $key => $value) {
                    array_push($params, $key);
                }

                $queryTelefone = "INSERT INTO telefone (";
                foreach ($params as $param) {
                    $queryTelefone = $queryTelefone . "$param, ";
                }

                $queryTelefone = $queryTelefone . "telefone_paciente ) VALUES (";

                foreach ($params as $param) {
                    $queryTelefone = $queryTelefone . ":$param, ";
                }
                $queryTelefone = $queryTelefone . ":telefone_paciente );";


                $stmtTelefone = $pdo->prepare($queryTelefone);

                foreach ($json_telefones as $telefone) {

                    foreach ($telefone as $key => $value) {
                        $stmtTelefone->bindValue(":$key", $value);
                    }
                    $stmtTelefone->bindValue(":telefone_paciente", $paciente_id);

                    if ($stmtTelefone->execute()) {
                        $lastID = $pdo->lastInsertID();
                        echo "Telefone cadastrado. ID: $lastID<br>";
                    } else {
                        echo "Erro na inserção de telefone<br>";
                        throw new Exception("Erro na inserção de telefone<br>");
                        $pdo->rollBack();
                    }
                }

                $pdo->commit();
            } else {
                throw new Exception("Algo deu errado na inserção do paciente");
                $pdo->rollBack();
            }
        }
    } catch (PDOEXception $e) {
        echo "<br>Gerenciando erro...<br>";
        echo $e->getMessage(); // exibe erro na tela
        exit();
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'GET') {
    if (empty($_GET['id'])) {
        $query = "SELECT * FROM paciente";
        $stmtEndereco = $pdo->prepare($query);
        $stmtEndereco->execute();
        $data = $stmtEndereco->fetchAll(PDO::FETCH_ASSOC);


        foreach ($data as $row) {
            echo json_encode($row);
        }

        $databaseErrors = $stmtEndereco->errorInfo();
        if (!empty($databaseErrors[2])) {
            $errorInfo = print_r($databaseErrors, true);
            $errorLogMsg = "error info: $errorInfo";
            echo $errorLogMsg;
        } else {
            echo $retorno->nome;
        }
    } else {
        $id = $_GET['id'];
        $query = "SELECT * FROM paciente WHERE id = :id";
        $stmtEndereco = $pdo->prepare($query);
        $stmtEndereco->bindValue(':id', $id);
        $stmtEndereco->execute();
        $data = $stmtEndereco->fetchAll(PDO::FETCH_ASSOC);

        if (!empty($data)) {
            foreach ($data as $row) {
                echo json_encode($row);
            }
        } else {
            echo "Nenhum resultado encontrado com o id $id";
        }
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'DELETE') {
    if (empty($_GET['id'])) {
        throw new Exception("Impossível apagar registro, é necessário informar um ID");
    } else {
        $id = $_GET['id'];
        $query = "DELETE FROM paciente WHERE id = :id";
        $stmtEndereco = $pdo->prepare($query);
        $stmtEndereco->bindValue(':id', $id);
        $stmtEndereco->execute();

        $count = $stmtEndereco->rowCount();
        if ($count > 0) {
            echo "$count registro deletado com o ID $id";
        } else {
            echo "Nenhum registro afetado";
        }
    }
} else if ($_SERVER['REQUEST_METHOD'] === 'PUT') {

    try {
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $pdo->beginTransaction();
        $json_str = file_get_contents('php://input');
        if (!empty($json_str)) {
            $json_obj = json_decode($json_str);
            $params = $json_obj->params;

            $query = "UPDATE paciente SET ";

            foreach ($params as $key => $value) {
                $query = $query . " $key = :$key,";
            }
            $query = rtrim($query, ',');
            $query = $query . " WHERE id = :id";

            $stmt = $pdo->prepare($query);
            foreach ($params as $key => $value) {
                $stmt->bindValue(":$key", $value);
            }

            $stmt->bindValue(":id", $json_obj->paciente_id);
            $time = date("Y-m-d h:i:s");
            $$stmt->execute();


            $count = $stmt->rowCount();
            if ($count > 0) {
                echo "<br><br>Registro atualizado com sucesso. $count linhas afetadas";
                $pdo->commit();
            } else {
                echo "<br><br>Nenhum registro afetado, algo deu errado.";
            }
        } else {
            $pdo->rollBack();
            throw new Exception("O JSON recebido está vazio");
        }
    } catch (PDOEXception $e) {
        echo "<br>Gerenciando erro...<br>";
        echo $e->getMessage(); // exibe erro na tela
        exit();
    }
} else {
    echo "<br>Tipo de requisição inválida. Nada Feito.";
}
